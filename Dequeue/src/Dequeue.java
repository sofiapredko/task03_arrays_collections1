import java.util.*;

public class Dequeue {
    public static void main(String[] args)
    {
        Deque<String> deque = new ArrayDeque<String>();
        //Deque<String> deque1 = new LinkedList<String>();

        deque.add("carrot");
        deque.addFirst("garlic");
        deque.addLast("potato");
        deque.push("mushrooms");
        deque.offer("cabbage");
        deque.offerFirst("onion");
        deque.offerLast("tomato");

        System.out.println(deque + "\n");

        System.out.println("Using Iterator");
        Iterator iterator = deque.iterator();
        while (iterator.hasNext())
            System.out.println("\t" + iterator.next());

        Iterator reverse = deque.descendingIterator();
        System.out.println("Using Reverse Iterator");
        while (reverse.hasNext())
            System.out.println("\t" + reverse.next());

        System.out.println("Peek(first element): " + deque.peek());
        System.out.println("After peek: " + deque);

        System.out.println("Pop (returns and removes first element) : " + deque.pop());
        System.out.println("After pop: " + deque);

        System.out.println("Contains element *garlic* : " + deque.contains("garlic"));

        System.out.println("Contains element *onionsss* : " + deque.contains("onionssss"));

        deque.removeFirst();
        deque.removeLast();

        System.out.println("Deque after removing " +
                "first and last elements: " + deque);

    }
}


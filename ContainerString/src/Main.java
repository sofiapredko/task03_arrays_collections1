
public class Main {

    public static void main(String[] args) {
        Container<String> stringContainer = new Container<>(String.class,3);

        System.out.println("String Container:");
        //System.out.println(stringContainer.size());
        stringContainer.add("1");
        stringContainer.add("2");
        stringContainer.add("3");
        stringContainer.add("5");

        System.out.println(stringContainer.toString());

        System.out.println();
        System.out.println("Comparing this perfomance with Arraylist obj: ");
        ArrList list = new ArrList();
        list.actionArrList();

    }


}

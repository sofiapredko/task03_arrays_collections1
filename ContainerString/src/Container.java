import java.lang.reflect.Array;
import java.util.Arrays;

public class Container<T extends String & Comparable<String>> {

    private T[] arr;
    private int nextInsertPosition;
    private Class<T> type;


    public Container(Class<T> c) {
        type = c;
        arr = (T[]) Array.newInstance(c, 5);
        nextInsertPosition = 0;
    }

    public Container(Class<T> c, int size) {
        type = c;
        arr = (T[]) Array.newInstance(c, size);
        nextInsertPosition = 0;
    }

    public int size() { return arr.length; }

    public T get(int i) {
        return arr[i];
    }

    public boolean set(int i, T e) {
        if (i < arr.length && i <= nextInsertPosition) {
            arr[i] = e;
            return true;
        }
        return false;
    }

    public void add(T e) {
        if (nextInsertPosition == arr.length) {
            T[] arrCopy = arr;
            arr = (T[]) Array.newInstance(type, arrCopy.length * 2);

            for (int i = 0; i < arrCopy.length; i++) {
                arr[i] = arrCopy[i];
            }
        }

        arr[nextInsertPosition] = e;

        nextInsertPosition++;
    }

    @Override
    public String toString() {
        return Arrays.toString(arr);
    }

}

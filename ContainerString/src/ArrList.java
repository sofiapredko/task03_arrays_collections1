import java.util.ArrayList;

public class ArrList {
    public void actionArrList() {

        ArrayList<String> alist = new ArrayList<String>();
        alist.add("Cake");
        alist.add("Sweets");
        alist.add("Rice");
        alist.add("Bread");
        alist.add("Juice");
        alist.add("Fruits");

        alist.add(0, "Vegetables");

        /*System.out.println("Print list:");
        for(String str: alist)
            System.out.println(str);
            */

        System.out.println(alist);

        alist.remove("Rice");
        alist.add("Drinks");
        alist.remove("Bread");

        System.out.println(alist);

        alist.remove(2);

        System.out.println(alist);
    }
}

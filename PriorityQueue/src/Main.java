public class Main {
    public static void main(String[] args) {
        Employee sofia = new Employee("Sofia", 2);
        Employee bohdan = new Employee("Bohdan", 5);
        Employee ira = new Employee("Ira", 1);

        PriorityQueue<Employee> employees = new PriorityQueue<>();

        employees.add(sofia);
        employees.add(bohdan);
        employees.add(ira);

        System.out.println(employees.toString());
        System.out.println(employees.peek());

        System.out.println(employees.contains(sofia));

        employees.poll();
        System.out.println(employees);


    }
}

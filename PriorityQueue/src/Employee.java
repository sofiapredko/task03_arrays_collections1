import java.util.Objects;

public class Employee implements Comparable<Employee>{
    private String name;

    @Override
    public int compareTo(Employee o) {
        return yearsOfExperience - o.yearsOfExperience;
    }

    private int yearsOfExperience;

    public Employee(String name, int yearsOfExperience) {
        this.name = name;
        this.yearsOfExperience = yearsOfExperience;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", yearsOfExperience=" + yearsOfExperience +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return getYearsOfExperience() == employee.getYearsOfExperience() &&
                Objects.equals(getName(), employee.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), getYearsOfExperience());
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

}

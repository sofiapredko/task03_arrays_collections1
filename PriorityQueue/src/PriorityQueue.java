import java.util.*;

public class PriorityQueue <T extends Comparable<T>> {
    private List<T> list;

    public PriorityQueue() {
        list = new ArrayList<>();
    }

    public PriorityQueue(T[] array) {
        list = Arrays.asList(array);
        Collections.sort(list);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (T i : list) {
            result.append(i.toString() + ", ");
        }
        return result.substring(0, result.length() - 2);
    }

    public void add(T item) {
        list.add(item);
        Collections.sort(list);
    }

    public void clear() {
        list.clear();
    }

    public boolean contains(T item) {
        return list.contains(item);
    }

    public void offer(T item) {
        add(item);
    }

    public T peek() {
        if (list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    public T poll() {
        if (list.size() == 0) {
            return null;
        }

        T result = list.get(0);
        list.remove(0);
        return result;
    }

    public void remote(T item) {
        list.remove(item);
    }

    public int size() {
        return list.size();
    }

    public T[] toArray() {
        return (T []) list.toArray();
    }
}

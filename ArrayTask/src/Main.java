import java.util.Scanner;

public class Main extends Array{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter size of arrays n = ");
        int n = scan.nextInt();
        int[] arr1 = new int[n];
        int[] arr2 = new int[n];

        create(arr1, n);
        create(arr2, n);
        print(arr1);
        print(arr2);

        System.out.print("Elements common in both arrays:  ");
        System.out.println(inBoth(arr1, arr2));
        System.out.print("Unique elements in both arrays:   ");
        System.out.println(findUnique(arr1, arr2));

        System.out.println("1 - delete all duplicates, 2 - delete serias of duplicates");
        System.out.print("Enter your choice: ");
        int choice = scan.nextInt();

        switch (choice){
            case 1:
                System.out.println("Delete duplicates in arrays: ");
                System.out.println(deleteDuplicate(arr1));
                System.out.println(deleteDuplicate(arr2));
                break;
            case 2:
                System.out.println("Delete serias of elements:  ");
                System.out.println(deleteRow(arr1));
                System.out.println(deleteRow(arr2));
        }
    }
}

import java.util.*;

public class Array {
    public static void create(int[] a, int n) {
        Random r = new Random();
        for (int i = 0; i < n; i++)
            a[i] = -10 + r.nextInt(21);
    }

    public static void print(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + "  ");
        }
        System.out.println();
    }

    public static Set<Integer> inBoth(int[] arr1, int[] arr2) {
        Set<Integer> list = new HashSet<Integer>();
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    list.add(arr1[i]);
                }
            }
        }
        return list;
    }

    public static HashSet<Integer> findUnique(int[] arr1, int[] arr2){
        Integer[] objectArray = new Integer[arr1.length];
        Integer[] objectArray1 = new Integer[arr2.length];

        for(int ctr1 = 0; ctr1 < arr1.length; ctr1++) {
            objectArray[ctr1] = Integer.valueOf(arr1[ctr1]);
        }

        for(int ctr2 = 0; ctr2 < arr2.length; ctr2++) {
            objectArray1[ctr2] = Integer.valueOf(arr2[ctr2]);
        }

        List<Integer> list1 = new ArrayList<>(Arrays.asList(objectArray));
        Set<Integer> set1 = new HashSet<>(list1);

        List<Integer> list2 = new ArrayList<>(Arrays.asList(objectArray1));
        Set<Integer> set2 = new HashSet<>(list2);

        set1.retainAll(set2);

        list1.removeAll(set1);
        list2.removeAll(set1);

        list1.addAll(list2);

        //System.out.println(new HashSet<>(list1));

        return ( new HashSet<>(list1));
    }


    public static Set<Integer> deleteDuplicate(int[] arr)  {
        Integer[] objectArray = new Integer[arr.length];

        for(int ctr = 0; ctr < arr.length; ctr++) {
            objectArray[ctr] = Integer.valueOf(arr[ctr]);
        }

        Set<Integer> linkedHashSet = new HashSet<>(Arrays.asList(objectArray));
        return linkedHashSet;
    }

    public static List<Integer> deleteRow(int[] arr){
        Integer[] values = new Integer[arr.length];

        for(int c = 0; c < arr.length; c++) {
            values[c] = Integer.valueOf(arr[c]);
        }


        List<Integer> list = new ArrayList<Integer>();
        list.addAll(Arrays.asList(values));

        for (int i = 0; i < list.size() - 1; i++) {
            while(list.get(i).equals(list.get(i+1)) && (i+1) < list.size()){
                list.remove(i+1);
            }
        }
        return list;
    }

}
